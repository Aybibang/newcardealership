const puppeteer = require('puppeteer');
let browser;
let page;
const url = 'http://172.16.4.182:7500/';

/////////////-----------------Customer can View list of cars and services in dashboard -----------------oki------------/////////////
test('TC_cust_01 Customer can view list of cars and available services', async() => {
    const browser = puppeteer.launch({headless: false, slowMo: 50})
    const page = (await browser).newPage()  

    await (await page).goto(url)
    await (await page).setViewport({ width: 1401, height: 928 })

    //Log in
    await (await page).waitForSelector('.username')
    await (await page).click('.username')
    await (await page).type('.username','customer') //ibutang niya ang ID
    await (await page).waitForSelector('.password')
    await (await page).click('.password') 
    await (await page).type('.password', '1234')
    await (await page).click('.btn.login', 30)

 //Wait for data to be displayed on table
 //    //navigate to service

        await (await page).waitForSelector('#Buy')
        await (await page).click('#Buy')
        await (await page).waitForSelector('#Service')
        await (await page).click('#Service')


  await (await page).waitForSelector('.container')
  const html = await (await page).$eval('.container', text => text.innerHTML);
  expect(html).toBeDefined();

  await (await browser).close()
}, 300000)

//---------------------Customer can search car ---------oki--------//

// test('TC_cust_02 Customer can search a cars', async() => {
//     const browser = puppeteer.launch({headless: false, slowMo: 50})
//     const page = (await browser).newPage()  

//     await (await page).goto(url)
//     await (await page).setViewport({ width: 1401, height: 928 })

//     //Log in
//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')
//     await (await page).type('.username','customer') //ibutang niya ang ID
//     await (await page).waitForSelector('.password')
//     await (await page).click('.password') 
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)


//     await (await page).waitForSelector('.row > .cardoutline:nth-child(1) > .card > .card-body > .view')
//     await (await page).click('.row > .cardoutline:nth-child(1) > .card > .card-body > .view')
//      await (await page).waitForSelector('.form-control')
//      await (await page).type('.form-control', 'Hilux')

//     await (await page).waitForSelector('.row > .cardoutline > .card > .card-body > h4')
//     const html = await (await page).$eval('.row > .cardoutline > .card > .card-body > h4', text => text.innerHTML);
//     expect(html).toBe('Hilux');
//     await (await browser).close()
// }, 300000)
//------------------------Customer cannot buy a car without car color---------ok--------------//
// test('TC_cust_03 Customer cannot buy a car without color', async() => {
//     const browser = puppeteer.launch({headless: false, slowMo: 50})
//     const page = (await browser).newPage()  

//     await (await page).goto(url)
//     await (await page).setViewport({ width: 1401, height: 928 })

//     //Log in
//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')
//     await (await page).type('.username','customer') //ibutang niya ang ID
//     await (await page).waitForSelector('.password')
//     await (await page).click('.password') 
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)

//     //Click sa Buy menu
//         await (await page).waitForSelector('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')
//         await (await page).click('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')

//         //Click Buy button
//         await (await page).waitForSelector('.row > .cardoutline:nth-child(1) > .card > .card-body > .btn:nth-child(4)')
//         await (await page).click('.row > .cardoutline:nth-child(1) > .card > .card-body > .btn:nth-child(4)')
        
//         //Click buy button
//         await (await page).waitForSelector('.btn.btn-primary.btn-lg')
//         await (await page).click('.btn.btn-primary.btn-lg')

//         //Choose Date
//         await (await page).waitFor('div #Date #Date')
//         await (await page).click('div #Date #Date')
//         await (await page).type('div #Date #Date', '11192020')

//         //Choose Time Schedule
//         await (await page).click('div #input-group-3 #input-3')
//         await (await page).type('div #input-group-3 #input-3','10:00 AM - 12:00 PM')
//         await (await page).click('div #input-group-3 #input-3')

//         //Click Submit button
//         await (await page).waitForSelector('.row > .col > div > form > .btn-primary')
//         await (await page).click('.row > .col > div > form > .btn-primary')

//         await (await page).waitForSelector('div > form > #input-group-3 > div > label')
//         const html = await (await page).$eval('div > form > #input-group-3 > div > label', text => text.innerHTML);
//         expect(html).toBe('Color required.');

//         await (await browser).close()
// }, 300000)

//--------------Customer cannot buy a car without date for repair service ------ok wala pa sa Test script-------//
// test('TC_cust_04 Customer cannot buy a car without date for repair service', async() => {
//     const browser = puppeteer.launch({headless: false, slowMo: 50})
//     const page = (await browser).newPage()  

//     await (await page).goto(url)
//     await (await page).setViewport({ width: 1401, height: 928 })

//     //Log in
//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')
//     await (await page).type('.username','customer') //ibutang niya ang ID
//     await (await page).waitForSelector('.password')
//     await (await page).click('.password') 
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)

//     //Click sa Buy menu
//         await (await page).waitForSelector('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')
//         await (await page).click('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')

//         //Click Buy button
//         await (await page).waitForSelector('.row > .cardoutline:nth-child(1) > .card > .card-body > .btn:nth-child(4)')
//         await (await page).click('.row > .cardoutline:nth-child(1) > .card > .card-body > .btn:nth-child(4)')

//         //Choose color
//         await (await page).click('#input-3')   
//         await (await page).type('#input-3', 'Red')
//         await (await page).click('#input-3')
        
//         //Click buy button
//         await (await page).waitForSelector('.btn.btn-primary.btn-lg')
//         await (await page).click('.btn.btn-primary.btn-lg')

//         //Choose Time Schedule
//         await (await page).click('div #input-group-3 #input-3')
//         await (await page).click('div #input-group-3 #input-3')

//         //Click Submit button
//         await (await page).waitForSelector('.row > .col > div > form > .btn-primary')
//         await (await page).click('.row > .col > div > form > .btn-primary')

//         await (await page).waitForSelector('div > form > #input-group-3 > div > label')
//         const html = await (await page).$eval('div > form > #input-group-3 > div > label', text => text.innerHTML);
//         expect(html).toBe('Date/Schedule Empty');

//         await (await browser).close()
// }, 300000)

//-------------Customer cannot buy car without time schedule------------------//
//  test('TC_cust_05 Customer can search a cars in dashboard', async() => {
//         const browser = puppeteer.launch({headless: false, slowMo: 50})
//         const page = (await browser).newPage()  
    
//         await (await page).goto(url)
//         await (await page).setViewport({ width: 1401, height: 928 })
    
//         //Log in
//         await (await page).waitForSelector('.username')
//         await (await page).click('.username')
//         await (await page).type('.username','customer') //ibutang niya ang ID
//         await (await page).waitForSelector('.password')
//         await (await page).click('.password') 
//         await (await page).type('.password', '1234')
//         await (await page).click('.btn.login', 30)
    
//         //Click sa Buy menu
//             await (await page).waitForSelector('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')
//             await (await page).click('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')
    
//             //Click Buy button
//             await (await page).waitForSelector('.row > .cardoutline:nth-child(1) > .card > .card-body > .btn:nth-child(4)')
//             await (await page).click('.row > .cardoutline:nth-child(1) > .card > .card-body > .btn:nth-child(4)')
    
//             //Choose color
//             await (await page).click('#input-3')   
//             await (await page).type('#input-3', 'Black')
//             await (await page).click('#input-3')
            
//             //Click buy button
//             await (await page).waitForSelector('.btn.btn-primary.btn-lg')
//             await (await page).click('.btn.btn-primary.btn-lg')
    
//             //Choose Date
//             await (await page).waitFor('div #Date #Date')
//             await (await page).click('div #Date #Date')
//             await (await page).type('div #Date #Date', '11192020')
    
//             //Click Submit button
//             await (await page).waitForSelector('.row > .col > div > form > .btn-primary')
//             await (await page).click('.row > .col > div > form > .btn-primary')
    
//             await (await page).waitForSelector('#delete-modal___BV_modal_content_ > #delete-modal___BV_modal_body_ > .d-block > .title > h2')
//             const html = await (await page).$eval('#delete-modal___BV_modal_content_ > #delete-modal___BV_modal_body_ > .d-block > .title > h2', text => text.innerHTML);
//             expect(html).toBe('APPOINTMENT SET!');
            
//             //Click Ok button
//             await (await page).waitForSelector('#delete-modal___BV_modal_content_ > #delete-modal___BV_modal_body_ > .d-block > a > .btn')
//             await (await page).click('#delete-modal___BV_modal_content_ > #delete-modal___BV_modal_body_ > .d-block > a > .btn')
//     }, 300000)

//----------------Customer cannot add new car without details---------------------//
// test('TC_cust_06 Customer cannot buy car without details', async() => {
//         const browser = puppeteer.launch({headless: false, slowMo: 50})
//         const page = (await browser).newPage()  
    
//         await (await page).goto(url)
//         await (await page).setViewport({ width: 1401, height: 928 })
    
//         //Log in
//         await (await page).waitForSelector('.username')
//         await (await page).click('.username')
//         await (await page).type('.username','customer') //ibutang niya ang ID
//         await (await page).waitForSelector('.password')
//         await (await page).click('.password') 
//         await (await page).type('.password', '1234')
//         await (await page).click('.btn.login', 30)
    
//         //Click sa Buy menu
//             await (await page).waitForSelector('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')
//             await (await page).click('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')
    
//             //Click Buy button
//             await (await page).waitForSelector('.row > .cardoutline:nth-child(1) > .card > .card-body > .btn:nth-child(4)')
//             await (await page).click('.row > .cardoutline:nth-child(1) > .card > .card-body > .btn:nth-child(4)')
    
//             //Choose color
           
//             //Click buy button
//             await (await page).waitForSelector('.btn.btn-primary.btn-lg')
//             await (await page).click('.btn.btn-primary.btn-lg')
    
//             //Choose Date
          
    
//             //Choose Time Schedule
       
//             //Click Submit button
//             await (await page).waitForSelector('.row > .col > div > form > .btn-primary')
//             await (await page).click('.row > .col > div > form > .btn-primary')
    
//             await (await page).waitForSelector('#delete-modal___BV_modal_content_ > #delete-modal___BV_modal_body_ > .d-block > .title > h2')
//             const html = await (await page).$eval('#delete-modal___BV_modal_content_ > #delete-modal___BV_modal_body_ > .d-block > .title > h2', text => text.innerHTML);
//             expect(html).toBe('APPOINTMENT SET!');
            
//             //Click Ok button
//             await (await page).waitForSelector('#delete-modal___BV_modal_content_ > #delete-modal___BV_modal_body_ > .d-block > a > .btn')
//             await (await page).click('#delete-modal___BV_modal_content_ > #delete-modal___BV_modal_body_ > .d-block > a > .btn')
//     }, 300000)


////--------------Customer can buy a car in CAR menu-------------//
// test('TC_cust_07 Customer can buy a cars in Car menu', async() => {
//     const browser = puppeteer.launch({headless: false, slowMo: 50})
//     const page = (await browser).newPage()  

//     await (await page).goto(url)
//     await (await page).setViewport({ width: 1401, height: 928 })

//     //Log in
//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')
//     await (await page).type('.username','customer') //ibutang niya ang ID
//     await (await page).waitForSelector('.password')
//     await (await page).click('.password') 
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)

//     //Click sa Buy menu
//         await (await page).waitForSelector('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')
//         await (await page).click('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')

//         //Click Buy button
//         await (await page).waitForSelector('.row > .cardoutline:nth-child(1) > .card > .card-body > .btn:nth-child(4)')
//         await (await page).click('.row > .cardoutline:nth-child(1) > .card > .card-body > .btn:nth-child(4)')

//         //Choose color
//         await (await page).click('#input-3')   
//         await (await page).type('#input-3', 'Black')
//         await (await page).click('#input-3')
        
//         //Click buy button
//         await (await page).waitForSelector('.btn.btn-primary.btn-lg')
//         await (await page).click('.btn.btn-primary.btn-lg')

//         //Choose Date
//         await (await page).waitFor('div #Date #Date')
//         await (await page).click('div #Date #Date')
//         await (await page).type('div #Date #Date', '11192020')

//         //Choose Time Schedule
//         await (await page).click('div #input-group-3 #input-3')
//         await (await page).type('div #input-group-3 #input-3','10:00 AM - 12:00 PM')
//         await (await page).click('div #input-group-3 #input-3')

//         //Click Submit button
//         await (await page).waitForSelector('.row > .col > div > form > .btn-primary')
//         await (await page).click('.row > .col > div > form > .btn-primary')

//         await (await page).waitForSelector('#delete-modal___BV_modal_content_ > #delete-modal___BV_modal_body_ > .d-block > .title > h2')
//         const html = await (await page).$eval('#delete-modal___BV_modal_content_ > #delete-modal___BV_modal_body_ > .d-block > .title > h2', text => text.innerHTML);
//         expect(html).toBe('APPOINTMENT SET!');
        
//         //Click Ok button
//         await (await page).waitForSelector('#delete-modal___BV_modal_content_ > #delete-modal___BV_modal_body_ > .d-block > a > .btn')
//         await (await page).click('#delete-modal___BV_modal_content_ > #delete-modal___BV_modal_body_ > .d-block > a > .btn')
// }, 300000)
//---------------------customer cannot avail a service without date--------------------------//
// test('TC_cust_08 Customer cannot avail a service without date of appointment', async() => {
//     const browser = puppeteer.launch({headless: false, slowMo: 50})
//     const page = (await browser).newPage()  

//     await (await page).goto(url)
//     await (await page).setViewport({ width: 1401, height: 928 })

//     //Log in
//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')
//     await (await page).type('.username','customer') //ibutang niya ang ID
//     await (await page).waitForSelector('.password')
//     await (await page).click('.password') 
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)

//    //navigate to service
//         await (await page).waitForSelector('#Service')
//         await (await page).click('#Service')

//         //click avail
//         await (await page).waitForSelector('.row > .cardoutline:nth-child(1) > .card > .card-body > .btn:nth-child(6)')
//         await (await page).click('.row > .cardoutline:nth-child(1) > .card > .card-body > .btn:nth-child(6)')


//         //Choose Date
//                  //Click AVAIL button
//             await (await page).waitForSelector('.row > .col > div > form > .btn')
//             await (await page).click('.row > .col > div > form > .btn')

//          //Choose Ok button
//             await (await page).waitForSelector('div > .row > .col-md-12 > a > .btn-success')
//             await (await page).click('div > .row > .col-md-12 > a > .btn-success')

//         await (await page).waitForSelector('div > form > #Date > div > label')
//         const html = await (await page).$eval('div > form > #Date > div > label', text => text.innerHTML);
//         expect(html).toBe('Date required.')
//         await (await browser).close()
// }, 500000)

//--------------Customer can avail a service---- Ok---//
// test('TC_cust_09 Customer can avail a service', async() => {
//     const browser = puppeteer.launch({headless: false, slowMo: 50})
//     const page = (await browser).newPage()  

//     await (await page).goto(url)
//     await (await page).setViewport({ width: 1401, height: 928 })

//     //Log in
//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')
//     await (await page).type('.username','customer') //ibutang niya ang ID
//     await (await page).waitForSelector('.password')
//     await (await page).click('.password') 
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)

//    //navigate to service
//         await (await page).waitForSelector('#Service')
//         await (await page).click('#Service')

//         //click avail
//         await (await page).waitForSelector('.row > .cardoutline:nth-child(1) > .card > .card-body > .btn:nth-child(6)')
//         await (await page).click('.row > .cardoutline:nth-child(1) > .card > .card-body > .btn:nth-child(6)')


//         //Choose Date
//         await (await page).click('#Date')   
//         await (await page).type('#Date', '12222020')
//         await (await page).click('#Date')
        
// //         //Click AVAIL button
//             await (await page).waitForSelector('.row > .col > div > form > .btn')
//             await (await page).click('.row > .col > div > form > .btn')

// //         //Choose Ok button
//             await (await page).waitForSelector('div > .row > .col-md-12 > a > .btn-success')
//             await (await page).click('div > .row > .col-md-12 > a > .btn-success')
//         //Data from Table to array
//         const data = await (await page).evaluate(() => {
//             const tds = Array.from(document.querySelectorAll('tr > .text-center:nth-child(1)'))
//             return tds.map(td => td.innerText)
//         });
//         expect (data[1]).toBe('2020-12-22');

//         await (await browser).close()
// }, 500000)
//----- Customer can print service ticket--------DI ma click ang print------//
// test('TC_cust_10 Customer can print a service', async() => {
//     const browser = puppeteer.launch({headless: false, slowMo: 50})
//     const page = (await browser).newPage()  

//     await (await page).goto(url)
//     await (await page).setViewport({ width: 1401, height: 928 })

//     //Log in
//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')
//     await (await page).type('.username','customer') //ibutang niya ang ID
//     await (await page).waitForSelector('.password')
//     await (await page).click('.password') 
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)

//    //navigate to service
//         await (await page).waitForSelector('#Service')
//         await (await page).click('#Service')

//         //click avail
//         await (await page).waitForSelector('.row > .cardoutline:nth-child(1) > .card > .card-body > .btn:nth-child(6)')
//         await (await page).click('.row > .cardoutline:nth-child(1) > .card > .card-body > .btn:nth-child(6)')


//         //Choose Date
//         await (await page).click('#Date')   
//         await (await page).type('#Date', '11/19/2020')
//         await (await page).click('#Date')
        
// //         //Click AVAIL button
//             await (await page).waitForSelector('.row > .col > div > form > .btn')
//             await (await page).click('.row > .col > div > form > .btn')

// //         //Choose Print button
//             await (await page).waitForSelector('.btn.btn-primary.btn-md')
//             await (await page).click('.btn.btn-primary.btn-md')

//             await (await page).click('.action-button')

//         await (await browser).close()
// }, 500000)

///--- Customer can Cancel an service appointment -------- Ok--------//////////////
// test('TC_cust_11 Customer can Cancel a service', async() => {
//      browser = puppeteer.launch({headless: false, slowMo: 50})
     
//      page = (await browser).newPage()  
//      await (await page).goto(url)

//     await (await page).setViewport({ width: 1401, height: 928 })

//     //Log in
//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')
//     await (await page).type('.username','customer') //ibutang niya ang ID
//     await (await page).waitForSelector('.password')
//     await (await page).click('.password') 
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)

//    //navigate to service
//         await (await page).waitForSelector('#Service')
//         await (await page).click('#Service')

//         //click avail
//         await (await page).waitForSelector('.row > .cardoutline:nth-child(1) > .card > .card-body > .btn:nth-child(6)')
//         await (await page).click('.row > .cardoutline:nth-child(1) > .card > .card-body > .btn:nth-child(6)')


//         //Choose Date
//         await (await page).click('#Date')   
//         await (await page).type('#Date', '12222020')
//         await (await page).click('#Date')
// //         //Click AVAIL button
//             await (await page).waitForSelector('.row > .col > div > form > .btn')
//             await (await page).click('.row > .col > div > form > .btn')

//             //Click Cancel button//  
     
//         await (await page).waitForSelector('#serviceTicket-modal___BV_modal_body_ > div > .row > .col-md-12 > .btn')
//          await (await page).click('#serviceTicket-modal___BV_modal_body_ > div > .row > .col-md-12 > .btn')

//         // Can go back to APPOINTMENT
//             await (await page).waitForSelector('.page-header > .bv-example-row > .row > .col > h1')
//     const html = await (await page).$eval('.page-header > .bv-example-row > .row > .col > h1', text => text.innerHTML);
//     expect(html).toBeDefined();
//         await (await browser).close()

// }, 50000)

//---------------CAR TRANSACTION------------//

/////////////---------------------Customer can view list of car ---------oki--------//
// test('TC_cust_12 Customer can view list of availble cars', async() => {
//     const browser = puppeteer.launch({headless: false, slowMo: 50})
//     const page = (await browser).newPage()  

//     await (await page).goto(url)
//     await (await page).setViewport({ width: 1401, height: 928 })

//     //Log in
//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')
//     await (await page).type('.username','customer') //ibutang niya ang ID
//     await (await page).waitForSelector('.password')
//     await (await page).click('.password') 
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)

//         //Click sa View more
//     await (await page).waitForSelector('.row > .cardoutline:nth-child(1) > .card > .card-body > .view')
//     await (await page).click('.row > .cardoutline:nth-child(1) > .card > .card-body > .view')
//     await (await page).waitForSelector('.carcards')
//     const html = await (await page).$eval('.carcards', text => text.innerHTML);
//     expect(html).toBeDefined();
//     await (await browser).close()
// }, 300000)

/////////////---------------------Customer can search car ---------oki--------//
// test('TC_cust_13 Customer can search a cars', async() => {
//     const browser = puppeteer.launch({headless: false, slowMo: 50})
//     const page = (await browser).newPage()  

//     await (await page).goto(url)
//     await (await page).setViewport({ width: 1401, height: 928 })

//     //Log in
//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')
//     await (await page).type('.username','customer') //ibutang niya ang ID
//     await (await page).waitForSelector('.password')
//     await (await page).click('.password') 
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)


//     await (await page).waitForSelector('.row > .cardoutline:nth-child(1) > .card > .card-body > .view')
//     await (await page).click('.row > .cardoutline:nth-child(1) > .card > .card-body > .view')
//      await (await page).waitForSelector('.form-control')
//      await (await page).type('.form-control', 'Urus')

//     await (await page).waitForSelector('.row > .cardoutline > .card > .card-body > h4')
//     const html = await (await page).$eval('.row > .cardoutline > .card > .card-body > h4', text => text.innerHTML);
//     expect(html).toBe('Urus');
//     await (await browser).close()
// }, 300000)

//--------- cannot buy car without car color-------------//
// test('TC_cust_14 Customer cannot buy a car without color', async() => {
//     const browser = puppeteer.launch({headless: false, slowMo: 50})
//     const page = (await browser).newPage()  

//     await (await page).goto(url)
//     await (await page).setViewport({ width: 1401, height: 928 })

//     //Log in
//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')
//     await (await page).type('.username','customer') //ibutang niya ang ID
//     await (await page).waitForSelector('.password')
//     await (await page).click('.password') 
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)

//     //Click sa Buy menu
//         await (await page).waitForSelector('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')
//         await (await page).click('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')

//         //Click Buy button
//         await (await page).waitForSelector('.row > .cardoutline:nth-child(1) > .card > .card-body > .btn:nth-child(4)')
//         await (await page).click('.row > .cardoutline:nth-child(1) > .card > .card-body > .btn:nth-child(4)')
        
//         //Click buy button
//         await (await page).waitForSelector('.btn.btn-primary.btn-lg')
//         await (await page).click('.btn.btn-primary.btn-lg')

//         //Choose Date
//         await (await page).waitFor('div #Date #Date')
//         await (await page).click('div #Date #Date')
//         await (await page).type('div #Date #Date', '06202022')

//         //Choose Time Schedule
//         await (await page).click('div #input-group-3 #input-3')
//         await (await page).type('div #input-group-3 #input-3','10:00 AM - 12:00 PM')
//         await (await page).click('div #input-group-3 #input-3')

//         //Click Submit button
//         await (await page).waitForSelector('.row > .col > div > form > .btn-primary')
//         await (await page).click('.row > .col > div > form > .btn-primary')

//         await (await page).waitForSelector('div > form > #input-group-3 > div > label')
//         const html = await (await page).$eval('div > form > #input-group-3 > div > label', text => text.innerHTML);
//         expect(html).toBe('Color required.');

//         await (await browser).close()
// }, 300000)
//----------------------Cannot buy new car without date for repair-----------------
// test('TC_cust_15 Customer cannot buy a cars without date', async() => {
//     const browser = puppeteer.launch({headless: false, slowMo: 50})
//     const page = (await browser).newPage()  


//     await (await page).goto(url)
//     await (await page).setViewport({ width: 1401, height: 928 })

//     //Log in
//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')
//     await (await page).type('.username','customer') //ibutang niya ang ID
//     await (await page).waitForSelector('.password')
//     await (await page).click('.password') 
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login')

//     //Click sa Buy menu
//         await (await page).waitForSelector('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')
//         await (await page).click('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')

//         //Click Buy button
//         await (await page).waitForSelector('.row > .cardoutline:nth-child(1) > .card > .card-body > .btn:nth-child(4)')
//         await (await page).click('.row > .cardoutline:nth-child(1) > .card > .card-body > .btn:nth-child(4)')

//         //Choose color
//         await (await page).click('#input-3')   
//         await (await page).type('#input-3', 'Red')
//         await (await page).click('#input-3')
        
//         //Click buy button
//         await (await page).waitForSelector('.btn.btn-primary.btn-lg')
//         await (await page).click('.btn.btn-primary.btn-lg')

  

//         //Choose Time Schedule
//         await (await page).click('div #input-group-3 #input-3')
    

//         //Click Submit button
//         await (await page).waitForSelector('.row > .col > div > form > .btn-primary')
//         await (await page).click('.row > .col > div > form > .btn-primary')

//         await (await page).waitForSelector('div > form > #input-group-3 > div > label')
//         const html = await (await page).$eval('div > form > #input-group-3 > div > label', text => text.innerHTML);
//         expect(html).toBe('Date/Schedule Empty');
// }, 300000)
///---------------------------------------customer cannot but without time sched--------------------------
// test('TC_cust_16 Customer cannot buy a cars without time', async() => {
//     const browser = puppeteer.launch({headless: false, slowMo: 50})
//     const page = (await browser).newPage()  


//     await (await page).goto(url)
//     await (await page).setViewport({ width: 1401, height: 928 })

//     //Log in
//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')
//     await (await page).type('.username','customer') //ibutang niya ang ID
//     await (await page).waitForSelector('.password')
//     await (await page).click('.password') 
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login')

//     //Click sa Buy menu
//         await (await page).waitForSelector('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')
//         await (await page).click('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')

//         //Click Buy button
//         await (await page).waitForSelector('.row > .cardoutline:nth-child(1) > .card > .card-body > .btn:nth-child(4)')
//         await (await page).click('.row > .cardoutline:nth-child(1) > .card > .card-body > .btn:nth-child(4)')

//         //Choose color
//         await (await page).click('#input-3')   
//         await (await page).type('#input-3', 'black')
//         await (await page).click('#input-3')
        
//         //Click buy button
//         await (await page).waitForSelector('.btn.btn-primary.btn-lg')
//         await (await page).click('.btn.btn-primary.btn-lg')

//         //Choose Date
//         await (await page).waitFor('div #Date #Date')
//         await (await page).click('div #Date #Date')
//         await (await page).type('div #Date #Date', '11192020')


//         //Click Submit button
//         await (await page).waitForSelector('.row > .col > div > form > .btn-primary')
//         await (await page).click('.row > .col > div > form > .btn-primary')

//         await (await page).waitForSelector('div > form > #input-group-3 > div > label')
//         const html = await (await page).$eval('div > form > #input-group-3 > div > label', text => text.innerHTML);
//         expect(html).toBe('Date/Schedule Empty');

// }, 300000)

///---------------------cannot buy car without details-------------------//
// test('TC_cust_17 Customer cannot buy a cars without details', async() => {
//         const browser = puppeteer.launch({headless: false, slowMo: 50})
//         const page = (await browser).newPage()  
    
    
//         await (await page).goto(url)
//         await (await page).setViewport({ width: 1401, height: 928 })
    
//         //Log in
//         await (await page).waitForSelector('.username')
//         await (await page).click('.username')
//         await (await page).type('.username','customer') //ibutang niya ang ID
//         await (await page).waitForSelector('.password')
//         await (await page).click('.password') 
//         await (await page).type('.password', '1234')
//         await (await page).click('.btn.login')
    
//         //Click sa Buy menu
//             await (await page).waitForSelector('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')
//             await (await page).click('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')
    
//             //Click Buy button
//             await (await page).waitForSelector('.row > .cardoutline:nth-child(1) > .card > .card-body > .btn:nth-child(4)')
//             await (await page).click('.row > .cardoutline:nth-child(1) > .card > .card-body > .btn:nth-child(4)')
    
//             //Choose color
//             await (await page).click('#input-3')   
         
//             //Click buy button
//             await (await page).waitForSelector('.btn.btn-primary.btn-lg')
//             await (await page).click('.btn.btn-primary.btn-lg')
    
//             //Choose Date
//             await (await page).waitFor('div #Date #Date')
           
    
//             //Click Submit button
//             await (await page).waitForSelector('.row > .col > div > form > .btn-primary')
//             await (await page).click('.row > .col > div > form > .btn-primary')
    
//             await (await page).waitForSelector('div > form > #input-group-3 > div > label')
//             const html = await (await page).$eval('div > form > #input-group-3 > div > label', text => text.innerHTML);
//             expect(html).toBe('Date/Schedule Empty');

//             await (await browser).close()
//     }, 300000)

//-----------------------------CUSTOMER CAN BUY CAR--------------OK------------
// test('TC_cust_18 Customer can buy a cars in dashboard', async() => {
//     const browser = puppeteer.launch({headless: false, slowMo: 50})
//     const page = (await browser).newPage()  


//     await (await page).goto(url)
//     await (await page).setViewport({ width: 1401, height: 928 })

//     //Log in
//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')
//     await (await page).type('.username','customer') //ibutang niya ang ID
//     await (await page).waitForSelector('.password')
//     await (await page).click('.password') 
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login')

//     //Click sa Buy menu
//         await (await page).waitForSelector('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')
//         await (await page).click('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')

//         //Click Buy button
//         await (await page).waitForSelector('.row > .cardoutline:nth-child(1) > .card > .card-body > .btn:nth-child(4)')
//         await (await page).click('.row > .cardoutline:nth-child(1) > .card > .card-body > .btn:nth-child(4)')

//         //Choose color
//         await (await page).click('#input-3')   
//         await (await page).type('#input-3', 'Red')
//         await (await page).click('#input-3')
        
//         //Click buy button
//         await (await page).waitForSelector('.btn.btn-primary.btn-lg')
//         await (await page).click('.btn.btn-primary.btn-lg')

//         //Choose Date
//         await (await page).waitFor('div #Date #Date')
//         await (await page).click('div #Date #Date')
//         await (await page).type('div #Date #Date', '11192020')

//         //Choose Time Schedule
//         await (await page).click('div #input-group-3 #input-3')
//         await (await page).type('div #input-group-3 #input-3','10:00 AM - 12:00 PM')
//         await (await page).click('div #input-group-3 #input-3')

//         //Click Submit button
//         await (await page).waitForSelector('.row > .col > div > form > .btn-primary')
//         await (await page).click('.row > .col > div > form > .btn-primary')

//         await (await page).waitForSelector('#delete-modal___BV_modal_content_ > #delete-modal___BV_modal_body_ > .d-block > .title > h2')
//         const html = await (await page).$eval('#delete-modal___BV_modal_content_ > #delete-modal___BV_modal_body_ > .d-block > .title > h2', text => text.innerHTML);
//         expect(html).toBe('APPOINTMENT SET!');
        
//         //Click Ok button
//         await (await page).waitForSelector('#delete-modal___BV_modal_content_ > #delete-modal___BV_modal_body_ > .d-block > a > .btn')
//         await (await page).click('#delete-modal___BV_modal_content_ > #delete-modal___BV_modal_body_ > .d-block > a > .btn')

// }, 300000)



//----------------------AAPPOINTMENT  TRANSACTION-------------Change data---------//


//--------------Customer can avail a service---- Ok---//
// test('TC_Cust_20 Customer can avail a service', async() => {
//     const browser = puppeteer.launch({headless: false, slowMo: 50})
//     const page = (await browser).newPage()  

//     await (await page).goto(url)
//     await (await page).setViewport({ width: 1401, height: 928 })

//     //Log in
//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')
//     await (await page).type('.username','customer') //ibutang niya ang ID
//     await (await page).waitForSelector('.password')
//     await (await page).click('.password') 
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)

//    //navigate to service
//         await (await page).waitForSelector('.navbar-nav > a:nth-child(3) > .nav-item > .nav-link > p')
//         await (await page).click('.navbar-nav > a:nth-child(3) > .nav-item > .nav-link > p')

//         //click avail
//         await (await page).waitForSelector('.row > .cardoutline:nth-child(1) > .card > .card-body > .btn:nth-child(6)')
//         await (await page).click('.row > .cardoutline:nth-child(1) > .card > .card-body > .btn:nth-child(6)')


//         //Choose Date
//         await (await page).click('#Date')   
//         await (await page).type('#Date', '12222020')
//         await (await page).click('#Date')
        
// //         //Click AVAIL button
//             await (await page).waitForSelector('.row > .col > div > form > .btn')
//             await (await page).click('.row > .col > div > form > .btn')

// //         //Choose Ok button
//             await (await page).waitForSelector('div > .row > .col-md-12 > a > .btn-success')
//             await (await page).click('div > .row > .col-md-12 > a > .btn-success')
//         //Data from Table to array
//         const data = await (await page).evaluate(() => {
//             const tds = Array.from(document.querySelectorAll('tr > .text-center:nth-child(1)'))
//             return tds.map(td => td.innerText)
//         });
//         expect (data[1]).toBe('2020-12-22');

//         await (await browser).close()
// }, 500000)

//----- Customer can print service ticket--------DI ma click ang print------//
// test('TC_cust_21 Customer can print a service ticket', async() => {
//     const browser = puppeteer.launch({headless: false, slowMo: 50})
//     const page = (await browser).newPage()  

//     await (await page).goto(url)
//     await (await page).setViewport({ width: 1401, height: 928 })

//     //Log in
//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')
//     await (await page).type('.username','customer') //ibutang niya ang ID
//     await (await page).waitForSelector('.password')
//     await (await page).click('.password') 
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)

//    //navigate to service tab
//         await (await page).waitForSelector('.navbar-nav > a:nth-child(3) > .nav-item > .nav-link > p')
//         await (await page).click('.navbar-nav > a:nth-child(3) > .nav-item > .nav-link > p')

//         //click avail
//         await (await page).waitForSelector('.row > .cardoutline:nth-child(1) > .card > .card-body > .btn:nth-child(6)')
//         await (await page).click('.row > .cardoutline:nth-child(1) > .card > .card-body > .btn:nth-child(6)')


//         //Choose Date
//         await (await page).click('#Date')   
//         await (await page).type('#Date', '11192022')
//         await (await page).click('#Date')
        
// //         //Click AVAIL button
//             await (await page).waitForSelector('.row > .col > div > form > .btn')
//             await (await page).click('.row > .col > div > form > .btn')

// //         //Choose Print button
//             await (await page).waitForSelector('.btn.btn-primary.btn-md')
//             await (await page).click('.btn.btn-primary.btn-md')

//             await (await page).click('.action-button')

//         await (await browser).close()
// }, 500000)

//---------------------------Cancel Service-----------------------------------//
// test('TC_Cust_22 Customer can Cancel a service', async() => {
//      browser = puppeteer.launch({headless: false, slowMo: 50})
     
//      page = (await browser).newPage()  
//      await (await page).goto(url)
 

  
//     await (await page).setViewport({ width: 1401, height: 928 })

//     //Log in
//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')
//     await (await page).type('.username','customer') //ibutang niya ang ID
//     await (await page).waitForSelector('.password')
//     await (await page).click('.password') 
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)

//    //navigate to service
//         await (await page).waitForSelector('.navbar-nav > a:nth-child(3) > .nav-item > .nav-link > p')
//         await (await page).click('.navbar-nav > a:nth-child(3) > .nav-item > .nav-link > p')

//         //click avail
//         await (await page).waitForSelector('.row > .cardoutline:nth-child(1) > .card > .card-body > .btn:nth-child(6)')
//         await (await page).click('.row > .cardoutline:nth-child(1) > .card > .card-body > .btn:nth-child(6)')


//         //Choose Date
//         await (await page).click('#Date')   
//         await (await page).type('#Date', '11192022')
//         await (await page).click('#Date')
// //         //Click AVAIL button
//             await (await page).waitForSelector('.row > .col > div > form > .btn')
//             await (await page).click('.row > .col > div > form > .btn')

//             //Click Cancel button//  
     
//         await (await page).waitForSelector('#serviceTicket-modal___BV_modal_body_ > div > .row > .col-md-12 > .btn')
//          await (await page).click('#serviceTicket-modal___BV_modal_body_ > div > .row > .col-md-12 > .btn')

//         // Can go back to APPOINTMENT
//         await (await page).waitForSelector('.page-header > .bv-example-row > .row > .col > h1')
//         const html = await (await page).$eval('.page-header > .bv-example-row > .row > .col > h1', text => text.innerHTML);
//         expect(html).toBeDefined();


//         await (await browser).close()

// }, 50000)


//-----------------MY TRANSACTION-------------Ok-------------//

// test('TC_Cust_23 Customer can View a car order', async() => {
//      browser = puppeteer.launch({headless: false, slowMo: 50})
     
//      page = (await browser).newPage()  
//      await (await page).goto(url)
//     await (await page).setViewport({ width: 1401, height: 928 })

//     //Log in
//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')
//     await (await page).type('.username','customer') //ibutang niya ang ID
//     await (await page).waitForSelector('.password')
//     await (await page).click('.password') 
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)

//     //Navigate MY TRANSACTION//
//     await (await page).waitForSelector('#navigation > .navbar-nav > #my-nav-dropdown > #my-nav-dropdown__BV_button_ > span')
//     await (await page).click('#navigation > .navbar-nav > #my-nav-dropdown > #my-nav-dropdown__BV_button_ > span')
        
//     await (await page).waitForSelector('.navbar-nav > #my-nav-dropdown > .dropdown-menu > li:nth-child(1) > .dropdown-item')
//     await (await page).click('.navbar-nav > #my-nav-dropdown > .dropdown-menu > li:nth-child(1) > .dropdown-item')

//      //Wait for data to be displayed on table
//     await (await page).waitForSelector('.table.b-table.table-sm')
//     const html = await (await page).$eval('.table.b-table.table-sm', text => text.innerHTML);
//     expect(html).toBeDefined();

//     await (await browser).close()
// }, 300000)

//-------------------Customer can Cancel a Car order----------//

// test('TC_cust_24 Customer can Cancel a Order in my transaction', async() => {
//      browser = puppeteer.launch({headless: false, slowMo: 50})   
//      page = (await browser).newPage()  
//      await (await page).goto(url)
//     await (await page).setViewport({ width: 1401, height: 928 })

//     //Log in
//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')
//     await (await page).type('.username','customer') //ibutang niya ang ID
//     await (await page).waitForSelector('.password')
//     await (await page).click('.password') 
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)

//     //Navigate MY TRANSACTION//
//     await (await page).waitForSelector('#navigation > .navbar-nav > #my-nav-dropdown > #my-nav-dropdown__BV_button_ > span')
//     await (await page).click('#navigation > .navbar-nav > #my-nav-dropdown > #my-nav-dropdown__BV_button_ > span')
        
//     await (await page).waitForSelector('.navbar-nav > #my-nav-dropdown > .dropdown-menu > li:nth-child(1) > .dropdown-item')
//     await (await page).click('.navbar-nav > #my-nav-dropdown > .dropdown-menu > li:nth-child(1) > .dropdown-item')

//     //Click Cancel button in table
//     await (await page).waitForSelector('.btn.mr-1.btn-danger.btn-sm')
//     await (await page).click('.btn.mr-1.btn-danger.btn-sm')

//     //Click Cancel button in MODAL
//     await (await page).waitForSelector('.modal-dialog > #cancel-modal___BV_modal_content_ > #cancel-modal___BV_modal_body_ > .text-center > .btn')
//     await (await page).click('.modal-dialog > #cancel-modal___BV_modal_content_ > #cancel-modal___BV_modal_body_ > .text-center > .btn')
  

//     //Assertion
//     await (await page).waitForSelector('#app > .wrapper > div > div > .alert')
//     await (await page).click('#app > .wrapper > div > div > .alert')


//    const data = await (await page).evaluate(() => {
//     let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//     return Invalid;})
//     expect(data).toMatch("Order has been canceled")

// }, 300000)

//----------------------------------------Customer can view requested service-------OK----------------------//

// test('TC_Cust_25 Customer view repair service', async() => {
//      browser = puppeteer.launch({headless: false, slowMo: 50})
     
//      page = (await browser).newPage()  
//      await (await page).goto(url)
//     await (await page).setViewport({ width: 1401, height: 928 })

//     //Log in
//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')
//     await (await page).type('.username','customer') //ibutang niya ang ID
//     await (await page).waitForSelector('.password')
//     await (await page).click('.password') 
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)


// //navigate to My repair Services//
//         await (await page).waitForSelector('#navigation > .navbar-nav > #my-nav-dropdown > #my-nav-dropdown__BV_button_ > span')
//         await (await page).click('#navigation > .navbar-nav > #my-nav-dropdown > #my-nav-dropdown__BV_button_ > span')
        
//         await (await page).waitForSelector('.navbar-nav > #my-nav-dropdown > .dropdown-menu > li:nth-child(2) > .dropdown-item')
//         await (await page).click('.navbar-nav > #my-nav-dropdown > .dropdown-menu > li:nth-child(2) > .dropdown-item')
//              //Wait for data to be displayed on table
//     await (await page).waitForSelector('.table.b-table.table-sm')
//     const html = await (await page).$eval('.table.b-table.table-sm', text => text.innerHTML);
//     expect(html).toBeDefined();

//     await (await browser).close()
// }, 300000)
       

//-----------------------------MY TRANSACTION Can cancel a service--------------------//

// test('TC_cust_26 Customer can cancel a Service in my Transaction', async() => {
//      browser = puppeteer.launch({headless: false, slowMo: 50})
     
//      page = (await browser).newPage()  
//      await (await page).goto(url)
//     await (await page).setViewport({ width: 1401, height: 928 })

//     //Log in
//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')
//     await (await page).type('.username','customer') //ibutang niya ang ID
//     await (await page).waitForSelector('.password')
//     await (await page).click('.password') 
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)


// //navigate to My repair Services//
//         await (await page).waitForSelector('#navigation > .navbar-nav > #my-nav-dropdown > #my-nav-dropdown__BV_button_ > span')
//         await (await page).click('#navigation > .navbar-nav > #my-nav-dropdown > #my-nav-dropdown__BV_button_ > span')
        
//         await (await page).waitForSelector('.navbar-nav > #my-nav-dropdown > .dropdown-menu > li:nth-child(2) > .dropdown-item')
//         await (await page).click('.navbar-nav > #my-nav-dropdown > .dropdown-menu > li:nth-child(2) > .dropdown-item')
            
//         //Click Cancel button in table
//         await (await page).waitForSelector('.btn.mr-1.btn-danger.btn-sm')
//         await (await page).click('.btn.mr-1.btn-danger.btn-sm')
//         //Click Cancel button in MODAL
//         await (await page).waitForSelector('.modal-dialog > #cancel-modal___BV_modal_content_ > #cancel-modal___BV_modal_body_ > .text-center > .btn')
//         await (await page).click('.modal-dialog > #cancel-modal___BV_modal_content_ > #cancel-modal___BV_modal_body_ > .text-center > .btn')
  

//         //Assertion
//         await (await page).waitForSelector('#app > .wrapper > div > div > .alert')
//         await (await page).click('#app > .wrapper > div > div > .alert')


//             const data = await (await page).evaluate(() => {
//                   let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//                          return Invalid;})
//                     expect(data).toMatch("Order has been canceled")

//     await (await browser).close()
// }, 300000)